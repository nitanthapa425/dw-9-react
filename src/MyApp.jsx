import React from "react";
import ReactRoutes from "./PracticeComponent/ReactRoutes";

const MyApp = () => {
  return (
    <div>
      <ReactRoutes></ReactRoutes>
    </div>
  );
};

export default MyApp;
