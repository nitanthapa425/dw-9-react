import React from "react";

const Detail = (props) => {
  console.log(props);
  return (
    <div>
      name is {props.name}
      <br></br>
      age is {props.age}
      <br></br>
      address is {props.address}
    </div>
  );
};

export default Detail;
