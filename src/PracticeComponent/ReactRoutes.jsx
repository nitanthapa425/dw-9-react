import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import ReadAllProduct from "./product/ReadAllProduct";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import ProductForm from "./product/ProductForm";
import UpdateProduct from "./product/UpdateProduct";
import ReadAllStudent from "./student/ReadAllStudent";
import ReadSpecificStudent from "./student/ReadSpecificStudent";
import StudentForm from "./student/StudentForm";
import UpdateStudent from "./student/UpdateStudent";
import MyLinks from "./MyLinks";
import Register from "./UserManagement/Register";
import VerifyEmail from "./UserManagement/VerifyEmail";
import AdminLogin from "./UserManagement/AdminLogin";

const ReactRoutes = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyLinks></MyLinks>

              <Outlet></Outlet>
              {/* <div>This is Footer</div> */}
            </div>
          }
        >
          <Route index element={<div> this is home page</div>}></Route>
          <Route
            path="verify-email"
            element={<VerifyEmail></VerifyEmail>}
          ></Route>
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProduct></ReadAllProduct>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>
            <Route path="create" element={<ProductForm></ProductForm>}></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route>
          </Route>

          <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudent></ReadAllStudent>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificStudent></ReadSpecificStudent>}
            ></Route>
            <Route path="create" element={<StudentForm></StudentForm>}></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route>
        </Route>
        <Route
          path="admin"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>admin dashboard</div>}></Route>
          <Route path="register" element={<Register></Register>}></Route>
          <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
        </Route>

        <Route path="*" element={<div>404 page</div>}></Route>
      </Routes>
    </div>
  );
};

export default ReactRoutes;

// /   => this is home page
// /products => read all products
// /products/:id => detail page
// /products/create => create products
// /products/update/:id => products update
