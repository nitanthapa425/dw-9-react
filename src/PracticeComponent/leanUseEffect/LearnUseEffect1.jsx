import React, { useEffect, useState } from "react";

const LearnUseEffect1 = () => {
  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(100); //101

  useEffect(() => {
    console.log("i am useEffect function");
  }, [count1, count2]);


  console.log("I am component");

  return (
    <div>
      count1 is {count1}
      <br></br>
      <button
        onClick={(e) => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <br></br>
      count2 is {count2}
      <br></br>
      <button
        onClick={(e) => {
          setCount2(count2 + 1);
        }}
      >
        Increment count2
      </button>
    </div>
  );
};

export default LearnUseEffect1;
