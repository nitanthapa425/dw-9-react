import React, { useEffect, useState } from "react";

const LearCleanUpFunction = () => {
  let [count, setCount] = useState(0);
  useEffect(() => {
    console.log("i am useEffect");

    return () => {
      console.log("i am cleanup function");
    };
  }, [count]);

  /* 
  clean up function is a function return by useEffect
  1st render (component did mount)
it does not execute in first render

 from 2nd render
 from 2nd render clean up function gets executed

 when useEffect function gets executed
 firs cleanup function execute then the code above return will execute

 component did unmount ( component remove)
 when component is removed (during hide and show) nothing gets execute but clean function will execute

   */
  return (
    <div>
      count1 is {count}
      <br></br>
      <button
        onClick={(e) => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearCleanUpFunction;
