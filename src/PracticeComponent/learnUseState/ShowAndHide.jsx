import React, { useState } from "react";

const ShowAndHide = () => {
  let [showImage, setShowImage] = useState(true);

  let handleImage = (isDisplay) => {
    return (e) => {
      setShowImage(isDisplay);
    };
  };
  return (
    <div>
      {showImage ? <img src="./logo192.png" alt="my logo"></img> : null}

      <br></br>
      <button onClick={handleImage(true)}>Show</button>
      <br></br>
      <button onClick={handleImage(false)}>Hide</button>
    </div>
  );
};

export default ShowAndHide;

// handleClick       (e)=>{}  //it is used if we don't have to pass value
//handeClick()         ()=>{return((e)=>{})}// it is used  if we need to pass some vaue
