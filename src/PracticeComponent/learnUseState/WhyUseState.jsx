// import React from "react";
import { useState } from "react";

// const WhyUseState = () => {
//   let name = "nitan";

//   return (
//     <div>
//       {name}
//       <br></br>
//       <button
//         onClick={(e) => {
//           name = "ram";
//         }}
//       >
//         Click
//       </button>
//     </div>
//   );
// };

// export default WhyUseState;

import React from "react";

const WhyUseState = () => {
  let [name, setName] = useState("nitan");
  console.log("********");
  return (
    <div>
      {name}
      <br />
      <button
        onClick={() => {
          setName("nitan");
        }}
      >
        Click
      </button>
    </div>
  );
};

//Browse

//clg
// *********
// *********
// *********

export default WhyUseState;
