import React, { useState } from "react";
// define variable using useState
// call variable
//change variable on buttton click

const LearnUseState1 = () => {
  // let name="nitan"

  let [name, setName] = useState("nitan");
  let [age, setAge] = useState(29);

  let handleClick = (e) => {
    // name= "ram" => it does not work
    setName("ram");
  };

  let handleAge = (e) => {
    setAge(30);
  };

  return (
    <div>
      my name is {name}
      <br></br>
      <button onClick={handleClick}>Change name</button>
      <br></br>
      my age is {age}
      <br></br>
      <button onClick={handleAge}>Change age</button>
    </div>
  );
};

export default LearnUseState1;
