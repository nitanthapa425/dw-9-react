import React, { useState } from "react";

const Increment = () => {
  let [count1, setCount1] = useState(0);
  //0//1//2//2//3
  let [count2, setCount2] = useState(100);
  //100//100//100//101//101
  return (
    <div>
      count1 is {count1}
      <br></br>
      count2 is {count2}
      <br></br>
      <button
        onClick={(e) => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <br></br>
      <button
        onClick={(e) => {
          setCount2(count2 + 1);
        }}
      >
        Increment count2
      </button>
    </div>
  );
};

export default Increment;
