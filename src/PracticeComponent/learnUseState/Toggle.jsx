import React, { useState } from "react";

const Toggle = () => {
  let [showImg, setShowImg] = useState(true);

  let handleClick = () => {
    if (showImg) {
      setShowImg(false);
    } else {
      setShowImg(true);
    }
  };
  return (
    <div>
      {showImg ? <img src="./logo192.png"></img> : null}

      <button onClick={handleClick}>{showImg ? "Hide" : "show"}</button>
    </div>
  );
};

export default Toggle;

//if seen(true) => setShowImg(false)
//if not seen(false)=> set showImg(true)

/* 

seen(true)  => Hide
seen(false) => Show

*/
