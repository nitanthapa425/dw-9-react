import React from "react";

const LeranTearnary = () => {
  let age = 19;

  let calc = () => {
    if (age < 18) {
      return <div>Underage</div>;
    } else if (age >= 18 && age <= 60) {
      return <div>Adult</div>;
    } else {
      return <div>Old</div>;
    }
  };

  return (
    <div>
      {age < 18 ? (
        <div>Underage</div>
      ) : age >= 18 && age < 60 ? (
        <div>Adult</div>
      ) : (
        <div>Old</div>
      )}

      {calc()}
    </div>
  );
};

export default LeranTearnary;
