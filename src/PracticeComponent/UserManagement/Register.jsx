import axios from "axios";
import React, { useState } from "react";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Register = () => {
  let [fullName, setFullName] = useState("");
  let [email, setEmail] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("");
  //   let [role, setRole] = useState("");
  let [password, setPassword] = useState(false);

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      email: email,
      password: password,
      //   role: role,
      dob: dob,
      gender: gender,
    };

    data = { ...data, role: "superadmin" };
    // console.log(data);
    let result = await axios({
      url: "http://localhost:8000/web-users",
      method: "POST",
      data: data,
    });
    console.log(result.data.message);
    toast.success(
      "Mail has been sent to your email. Please verify your account by clicking the link on your mail"
    );
    setFullName("");
    setEmail("");
    setPassword("");
    setDob("");
    setGender("");
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  //   let result =axios({})
  // toast("Wow so easy!");
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="fullName">Full Name: </label>
          <input
            type="text"
            // placeholder="Eg: Gopal"
            value={fullName} //abc
            onChange={(e) => {
              setFullName(e.target.value);
            }}
            id="fullName"
          ></input>
        </div>
        <div>
          <label htmlFor="email">Email: </label>
          <input
            type="email"
            // placeholder="1000"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            id="email"
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            value={password} //abc
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            id="password"
          ></input>
        </div>
        <div>
          <label htmlFor="dob">DOB: </label>
          <input
            type="date"
            value={dob} //abc
            onChange={(e) => {
              setDob(e.target.value);
            }}
            id="dob"
          ></input>
        </div>
        <div>
          <label>Gender</label>
          {/* 
          <label htmlFor="male">Male </label>
          <input
            type="radio"
            value="male"
            id="male"
            checked={gender === "male"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>

          <label htmlFor="female">Female</label>
          <input
            type="radio"
            value="female"
            id="female"
            checked={gender === "female"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>

          <label htmlFor="other">Other</label>
          <input
            type="radio"
            value="other"
            id="other"
            checked={gender === "other"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input> */}

          {/* {label:"Other", value:"other"}, */}

          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>
        {/* <div>
          <label htmlFor="role">Role: </label>
          <input
            type="text"
            value={role} //abc
            onChange={(e) => {
              setRole(e.target.value);
            }}
            id="role"
          ></input>
        </div> */}

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default Register;

// name type = text
// lastName type = text
// email type = email
// password type= password
// dob type ="date"
//isMarried  => type checkbox

//select [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]

// gender type= "radio"

// other     =>  value ,     e.target.value
//Checkbox   =>  checked,     e.targe.checked
//Radiobutton => checked,     e.target.value

//useRef
