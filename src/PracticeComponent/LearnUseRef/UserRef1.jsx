import React, { useRef } from "react";

// useRef

const UserRef1 = () => {
  let ref1 = useRef();
  let ref2 = useRef();
  let ref3 = useRef();
  return (
    <div>
      {/* <label htmlFor="name">Name</label>
      <input id="name"></input> */}

      <button
        onClick={() => {
          ref3.current.style.backgroundColor = "red";
          ref3.current.focus();
        }}
      >
        Color 3
      </button>

      <button
        onClick={() => {
          ref3.current.focus();
        }}
      >
        Focus 3
      </button>
      <button
        onClick={() => {
          ref3.current.blur();
        }}
      >
        Blur 3
      </button>
      <input ref={ref1}></input>
      <input ref={ref2}></input>
      <input ref={ref3}></input>
    </div>
  );
};

export default UserRef1;
