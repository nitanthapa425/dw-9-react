import { configure } from "@testing-library/react";
import React, { useState } from "react";

const Form1 = () => {
  let [name, setName] = useState("");
  let [email, setEmail] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [day, setDay] = useState("day1");
  let [gender, setGender] = useState("male");
  let onSubmit = (e) => {
    e.preventDefault();

    let data = {
      name: name,
      email: email,
      isMarried: isMarried,
      day: day,
      gender: gender,
    };

    console.log(data);
  };

  let days = [
    {
      label: "Sunday",
      value: "day1",
    },
    {
      label: "Monday",
      value: "day2",
    },
    {
      label: "Tuesday",
      value: "day3",
    },
    {
      label: "Wednesday",
      value: "day4",
    },
    {
      label: "Thursday",
      value: "day5",
    },
    {
      label: "Friday",
      value: "day6",
    },
    {
      label: "Saturday",
      value: "day7",
    },
  ];

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            placeholder="Eg: Gopal"
            value={name} //abc
            onChange={(e) => {
              setName(e.target.value);
            }}
            id="name"
          ></input>
        </div>
        <div>
          <label htmlFor="email">Email: </label>
          <input
            type="email"
            placeholder="Eg:  abc@gmail.com"
            value={email} //abc
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            id="email"
          ></input>
        </div>

        {/* 
        value, e.target.value
         */}
        <div>
          <label>IsMarried: </label>
          <input
            type="checkbox"
            checked={isMarried === true}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="day">Day: </label>
          <select
            value={day} //"day4"
            onChange={(e) => {
              setDay(e.target.value);
            }}
            id="day"
          >
            {days.map((item, i) => {
              return <option value={item.value}>{item.label}</option>;
            })}

            {/* <option value="day1">Sunday</option>
            <option value="day2">Monday</option>
            <option value="day3">Tuesday</option>
            <option value="day4">Wednesday</option>
            <option value="day5">Thursday</option>
            <option value="day6">Friday</option>
            <option value="day7">Saturday</option> */}
          </select>
        </div>

        <div>
          <label>Gender</label>
          {/* 
          <label htmlFor="male">Male </label>
          <input
            type="radio"
            value="male"
            id="male"
            checked={gender === "male"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>

          <label htmlFor="female">Female</label>
          <input
            type="radio"
            value="female"
            id="female"
            checked={gender === "female"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>

          <label htmlFor="other">Other</label>
          <input
            type="radio"
            value="other"
            id="other"
            checked={gender === "other"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input> */}

          {/* {label:"Other", value:"other"}, */}

          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default Form1;

// name type = text
// lastName type = text
// email type = email
// password type= password
// dob type ="date"
//isMarried  => type checkbox

//select [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]

// gender type= "radio"

// other     =>  value ,     e.target.value
//Checkbox   =>  checked,     e.targe.checked
//Radiobutton => checked,     e.target.value
