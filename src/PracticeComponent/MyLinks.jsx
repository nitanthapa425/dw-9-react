import React from "react";
import { NavLink } from "react-router-dom";

const MyLinks = () => {
  return (
    <div>
      {/* <a href="http://localhost:3000/products/create">Create Product</a> */}

      {/* <NavLink
        to="http://localhost:3000/products/create"
        style={{ marginRight: "20px" }}
      >
        Create Product
      </NavLink> */}

      {/* <a
        href="http://localhost:3000/products/create"
        style={{ marginRight: "20px" }}
      >
        Create Product
      </a> */}

      {/* <NavLink
        to="http://localhost:3000/products"
        style={{ marginRight: "20px" }}
      >
        Product
      </NavLink>

      <NavLink
        to="http://localhost:3000/students/create"
        style={{ marginRight: "20px" }}
      >
        Create Student
      </NavLink>
      <NavLink
        to="http://localhost:3000/students"
        style={{ marginRight: "20px" }}
      >
        Student
      </NavLink> */}
      <NavLink
        to="http://localhost:3000/admin/register"
        style={{ marginRight: "20px" }}
      >
        Register
      </NavLink>
    </div>
  );
};

export default MyLinks;

/* 

Create Product  => "http://localhost:3000/products/create"
Product   =>  "http://localhost:3000/products"


*/
