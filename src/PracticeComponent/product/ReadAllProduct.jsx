import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);

const ReadAllProduct = () => {
  let [products, setProducts] = useState([]);
  let navigate = useNavigate();

  let getAllProduct = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/products",
        method: "get",
      });

      setProducts(result.data.result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    getAllProduct();
  }, []);

  // let handleNavigate = (e) => {
  //   navigate(`/products/${item._id}`);
  // }

  let handleNavigate = (id) => {
    return (e) => {
      navigate(`/products/${id}`);
    };
  };

  let handleUpdate = (id) => {
    return (e) => {
      navigate(`/products/update/${id}`);
    };
  };

  let handleDelete = (id) => {
    return (e) => {
      MySwal.fire({
        title: "Confirmation",
        text: "Are you sure you want Delete?",
        showCancelButton: true,
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
      }).then(async (result) => {
        if (result.isConfirmed) {
          try {
            let result = await axios({
              url: `http://localhost:8000/products/${id}`,
              method: "DELETE",
            });
            getAllProduct();
            toast.success(result.data.message);
          } catch (error) {
            toast.error(error.response.data.message);
          }
        } else if (result.isDismissed) {
          console.log("cancel button is clicked");
        }
      });
    };
  };

  return (
    <>
      <ToastContainer></ToastContainer>
      {products.map((item, i) => {
        console.log(item);
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "20px" }}
          >
            <p>Product is {item.name}</p>
            <p>Product price NRs. {item.price}</p>
            <p>Product quantity {item.quantity}</p>
            <button
              style={{ marginRight: "20px" }}
              onClick={handleUpdate(item._id)}
            >
              Edit
            </button>
            <button
              style={{ marginRight: "20px" }}
              onClick={handleNavigate(item._id)}
            >
              View
            </button>
            <button
              style={{ marginRight: "20px" }}
              onClick={handleDelete(item._id)}
            >
              Delete
            </button>
          </div>
        );
      })}
    </>
  );
};

export default ReadAllProduct;

// Component
// A
// B
// C
// D

// DB
// B
// C
// D
