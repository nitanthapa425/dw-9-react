import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateProduct = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState(false);

  let params = useParams();
  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      price: price,
      quantity: quantity,
    };

    // console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/products/${params.id}`,
        method: "PATCH",
        data: data,
      });
      navigate(`/products/${params.id}`);
    } catch (error) {
      // error will only shown in mozilla browser
      toast.error(error.response.data.message);
    }
  };
  // toast("Wow so easy!");

  let getProduct = async () => {
    let result = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: "GET",
    });

    // setProduct(result.data.result);
    let data = result.data.result;
    setName(data.name);
    setPrice(data.price);
    setQuantity(data.quantity);
  };

  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            placeholder="Eg: Gopal"
            value={name} //abc
            onChange={(e) => {
              setName(e.target.value);
            }}
            id="name"
          ></input>
        </div>
        <div>
          <label htmlFor="price">Price: </label>
          <input
            type="number"
            placeholder="1000"
            value={price}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
            id="price"
          ></input>
        </div>
        <div>
          <label htmlFor="quantity">Quantity: </label>
          <input
            type="number"
            value={quantity} //abc
            onChange={(e) => {
              setQuantity(e.target.value);
            }}
            id="quantity"
          ></input>
        </div>

        <button type="submit">Update</button>
      </form>
    </div>
  );
};

export default UpdateProduct;

// name type = text
// lastName type = text
// email type = email
// password type= password
// dob type ="date"
//isMarried  => type checkbox

//select [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]

// gender type= "radio"

// other     =>  value ,     e.target.value
//Checkbox   =>  checked,     e.targe.checked
//Radiobutton => checked,     e.target.value

/* 
http://localhost:8000/products//id
method:PATCH


*/
