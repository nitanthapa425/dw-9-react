import axios from "axios";
import React, { useState } from "react";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ProductForm = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState(false);

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      price: price,
      quantity: quantity,
    };

    console.log(data);
  };
  // toast("Wow so easy!");
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            placeholder="Eg: Gopal"
            value={name} //abc
            onChange={(e) => {
              setName(e.target.value);
            }}
            id="name"
          ></input>
        </div>
        <div>
          <label htmlFor="price">Price: </label>
          <input
            type="number"
            placeholder="1000"
            value={price}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
            id="price"
          ></input>
        </div>
        <div>
          <label htmlFor="quantity">Quantity: </label>
          <input
            type="number"
            value={quantity} //abc
            onChange={(e) => {
              setQuantity(e.target.value);
            }}
            id="quantity"
          ></input>
        </div>

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default ProductForm;

// name type = text
// lastName type = text
// email type = email
// password type= password
// dob type ="date"
//isMarried  => type checkbox

//select [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]

// gender type= "radio"

// other     =>  value ,     e.target.value
//Checkbox   =>  checked,     e.targe.checked
//Radiobutton => checked,     e.target.value

//useRef
