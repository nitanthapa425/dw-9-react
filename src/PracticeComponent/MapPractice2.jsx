import React from "react";
import { products } from "../productData";

const MapPractice2 = () => {
  let task1 = () => {
    let desireOutput = products.map((item, i) => {
      return <p>{item.title}</p>;
    });

    return desireOutput;
  };
  let task2 = () => {
    let desireOutput = products.map((item, i) => {
      return (
        <p>
          {item.title} costs NRs.{item.price}
        </p>
      );
    });

    return desireOutput;
  };
  let task3 = () => {
    let desireOutput = products
      .filter((item, i) => {
        if (item.category === "Books") {
          return true;
        }
      })
      .map((item, i) => {
        return (
          <p>
            {item.title} costs NRS.{item.price} and its category is{" "}
            {item.category}
          </p>
        );
      });

    return desireOutput;
  };
  let task4 = () => {
    let productPrice = products
      .map((item, i) => {
        return item.price;
      })
      .reduce((pre, cur) => {
        return pre + cur;
      }, 0);
    return productPrice;
  };

  return (
    <div>
      {/* <h1>The product in our shop</h1> */}
      {/* {task1()} */}
      {/* {task2()} */}
      {/* {task3()} */}
      <div>the total price of all products is NRs. {task4()}</div>
    </div>
  );
};

export default MapPractice2;
