import { useState } from "react";
import "./App.css";
import Age from "./PracticeComponent/Age";
import ButtonClick from "./PracticeComponent/ButtonClick";
import Detail from "./PracticeComponent/Detail";
import DifferentDataEffect from "./PracticeComponent/DifferentDataEffect";
import LeranTearnary from "./PracticeComponent/LeranTearnary";
import MapPractice from "./PracticeComponent/MapPractice";
import MapPractice2 from "./PracticeComponent/MapPractice2";
import Name from "./PracticeComponent/Name";
import LearCleanUpFunction from "./PracticeComponent/leanUseEffect/LearCleanUpFunction";
import LearnUseEffect1 from "./PracticeComponent/leanUseEffect/LearnUseEffect1";
import LearnUseEffect2 from "./PracticeComponent/leanUseEffect/LearnUseEffect2";
import Increment from "./PracticeComponent/learnUseState/Increment";
import LearnUseState1 from "./PracticeComponent/learnUseState/LearnUseState1";
import ShowAndHide from "./PracticeComponent/learnUseState/ShowAndHide";
import Toggle from "./PracticeComponent/learnUseState/Toggle";
import WhyUseState from "./PracticeComponent/learnUseState/WhyUseState";
import MyLinks from "./PracticeComponent/MyLinks";
import MyRoutes from "./PracticeComponent/MyRoutes";
import ReactRoutes from "./PracticeComponent/ReactRoutes";
import Form1 from "./PracticeComponent/Form/Form1";
import UserRef1 from "./PracticeComponent/LearnUseRef/UserRef1";

// inline element
// block element
function App() {
  let tag1 = <div>this is a div</div>;
  let a = 1;
  let b = 2;

  let [showCom, setShowCom] = useState(true);
  return (
    <div>
      {/* <div style={{ color: "red", backgroundColor: "#000000" }}>
        My name is nitan thapa
      </div>
      <div className="success">class name dw9</div>
      <div className="failure">class name dw8</div>
      {tag1}
      {a + b} */}
      {/* 
      <div style={{ color: "red" }}>this is div</div>
      <a href="#"> click me</a> */}

      {/* <Name></Name> */}
      {/* <Age></Age> */}
      {/* <Detail name="nitan" age={29} address="gagalphedi"></Detail> */}
      {/* <LeranTearnary></LeranTearnary> */}
      {/* <DifferentDataEffect></DifferentDataEffect> */}
      {/* <MapPractice></MapPractice> */}
      {/* <MapPractice2></MapPractice2> */}
      {/* <ButtonClick></ButtonClick> */}

      {/* <LearnUseState1></LearnUseState1> */}
      {/* <ShowAndHide></ShowAndHide> */}
      {/* <Toggle></Toggle> */}
      {/* <WhyUseState></WhyUseState> */}
      {/* <Increment></Increment> */}

      {/* <LearnUseEffect1></LearnUseEffect1> */}

      {/* <LearnUseEffect2></LearnUseEffect2> */}

      {/* {showCom ? <LearCleanUpFunction></LearCleanUpFunction> : null}

      <button
        onClick={() => {
          setShowCom(true);
        }}
      >
        Show
      </button>
      <button
        onClick={() => {
          setShowCom(false);
        }}
      >
        Hide
      </button> */}

      {/* <MyRoutes></MyRoutes> */}
      {/* <ReactRoutes></ReactRoutes> */}
      {/* <Form1></Form1> */}
      {/* <UserRef1></UserRef1> */}
    </div>
  );
}

export default App;
